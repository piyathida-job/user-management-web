const express = require('express')
const mysql = require('mysql')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')
const alert = require('alert')
const app = express()
app.use(bodyParser.json())
app.use(morgan('dev'))
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.engine('html', require('ejs').renderFile)
app.set('view engine', 'html')
app.use(express.static(__dirname))
app.use('/images', express.static(__dirname + '/img'))
// declare variable
let name
let surname
let id
let position
let salary
let total_sale
let login
let count
let logout

const con = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'dcoffee'
})

con.connect(err => {
  if (err) {
    console.log('Error connecting to Db ' + err)
    res.send('Error connecting to Db ' + err)
    return
  }
  console.log('Connection established')
})

app.listen(3000, function () {
  console.log('Server Listen at http://localhost:3000')
})
app.get('/', function (req, res, next) {
  res.sendFile(__dirname + '/html/' + 'login.html')
})
app.post('/check', function (req, res, next) {
  user = req.body.email
  password = req.body.password
  console.log(user + ' ' + password)
  con.query(
    'SELECT mst_employee.name, mst_employee.surname,mst_employee.id_employee, mst_employee.position, FORMAT(mst_employee.salary, 2) AS salary, FORMAT(mst_employee.total_sale, 2) as total_sale FROM mst_security INNER JOIN mst_employee ON mst_security.id_employee = mst_employee.id_employee WHERE USER = ? AND PASSWORD = ? ',
    [user, password],
    (err, result) => {
      if (err) throw err
      if (result.length > 0) {
        name = result[result.length - 1].name
        surname = result[result.length - 1].surname
        id = result[result.length - 1].id_employee
        position = result[result.length - 1].position
        salary = result[result.length - 1].salary
        total_sale = result[result.length - 1].total_sale
        con.query(
          'SELECT datetime_logout FROM trn_logout INNER JOIN mst_security ON trn_logout.id_employee = mst_security.id_employee WHERE USER = ? AND PASSWORD = ?',
          [user, password],
          (err, result) => {
            if (err) {
              throw err
            }
            if (result.length > 0) {
              logout = (result[result.length - 1].datetime_logout)
            }
          }
        )

        con.query(
          'SELECT COUNT(trn_login.id_login) AS countLogin FROM trn_login INNER JOIN mst_security ON trn_login.id_employee = mst_security.id_employee WHERE mst_security.USER = ? AND mst_security.PASSWORD = ?',
          [user, password],
          (err, result2) => {
            if (err) throw err
            if (result2.length > 0) {
              count = result2[result2.length - 1].countLogin
              con.query(
                'SELECT datetime_login FROM trn_login INNER JOIN mst_security ON trn_login.id_employee = mst_security.id_employee WHERE mst_security.USER = ? AND mst_security.PASSWORD = ?',
                [user, password],
                (err, result3) => {
                  if (err) throw err
                  if (result3.length > 0) {
                    login =
                      result3[result3.length - 1].datetime_login

                  }
                }
              )
            }

            let date_ob = new Date()
            var loginTime = {
              datetime_login: (date_ob),
              id_employee: id
            }
            con.query('INSERT INTO trn_login SET ?', loginTime, (err, res2) => {
              if (err) throw err
              console.log('Last insert ID:', res2.insertId)
            })
          }
        )

        if (position === 'เจ้าของร้าน') {
          res.redirect('/main')
        } else if (position === 'พนักงาน') {
          res.redirect('/main_emp')
        }
      } else {
        alert('คุณไม่เป็นสมาชิก')
        res.redirect('/')
      }
    }
  )
})
app.get('/main', function (req, res, next) {
  res.render(__dirname + '/html/main.html', {
    name: name,
    surname: surname
  })
})
app.get('/main_emp', function (req, res, next) {
  res.render(__dirname + '/html/main_emp.html', {
    name: name,
    surname: surname
  })
})
app.get('/logout', function (req, res, next) {
  res.sendFile(__dirname + '/html/' + 'login.html')
  let date_ob = new Date()
  var logoutTime = { datetime_logout: (date_ob), id_employee: id }
  con.query('INSERT INTO trn_logout SET ?', logoutTime, (err, res) => {
    if (err) throw err
    console.log('Last insert ID:', res.insertId)
  })
  name = ''
  surname = ''
  id = ''
  position = ''
  salary = ''
  total_sale = ''
  login = ''
  logout = ''
  res.redirect('/')
})

app.get('/memberControl.html', function (req, res, next) {
  console.log("here memberControl")
  con.query(
    'SELECT id_employee, name, surname, position, FORMAT(salary,2) AS salary , FORMAT(total_sale,2) as total_sale FROM `mst_employee`',
    (err, data) => {
      if (err) throw err
      console.log(data)
      res.render(__dirname + '/html/memberControl.html', { data: data })
    }
  )
})
app.get('/addmember.html', function (req, res, next) {
  res.sendFile(__dirname + '/html/' + 'addmember.html')
})
app.post('/addMember', function (req, res, next) {
  var employee = {
    name: req.body.name,
    surname: req.body.surname,
    position: req.body.position,
    salary: req.body.salary,
    total_sale: req.body.total_sale
  }
  con.query('INSERT INTO mst_employee SET ?', employee, (err, res2) => {
    if (err) throw err
    console.log('Last insert ID:', res2.insertId)
    insert = res2.insertId
    var security = {
      id_security:res2.insertId,
      user: req.body.email,
      password: req.body.password,
      id_employee: res2.insertId
    }
    con.query('INSERT INTO mst_security SET ?', security, (err, res3) => {
      if (err) throw err
      console.log('Last insert ID:', res3.insertId)
      return res.status(201)
    })
  })
  res.redirect('/main')
})
app.get('/search/:va', function (req, res, next) {
  let { va } = req.params
  var search
  search = "%" + va + "%"
  console.log(search)
  var len = search.length
  con.query(
    'SELECT id_employee, name, surname, position, FORMAT(salary,2) AS salary , FORMAT(total_sale,2) as total_sale FROM `mst_employee` where name like ? or surname like ? or position like ?',
    [search, search, search],
    (err, data) => {
      if (err) throw err
      console.log(data)
      res.render(__dirname + '/html/memberControl.html', { data: data })
    }
  )
})


app.get('/editmember.html', function (req, res, next) {
  res.sendFile(__dirname + '/html/' + 'editmember.html')
})
var search
app.get('/edit/:idd', function (req, res, next) {
  let { idd } = req.params
  search = idd
  con.query(
    'SELECT mst_employee.id_employee, name, surname, position, FORMAT(salary,2) AS salary , FORMAT(total_sale,2) as total_sale, user, password FROM  mst_employee INNER JOIN mst_security ON mst_employee.id_employee = mst_security.id_employee  where mst_employee.id_employee = ? ',
  [search],
    (err, datag) => {
      if (err) throw err
      console.log(datag)
      res.render(__dirname + '/html/editmember.html', { 
        datag: datag,
        salary:datag[0].salary, 
        total_sale:datag[0].total_sale })
    }
  )
})
app.post('/editmember', function (req, res1, next) {
    c= req.body.name,
    s= req.body.surname,
    position= req.body.position,
    salary= req.body.salary,
    total_sale= req.body.total_sale,
    user = req.body.email,
    pass = req.body.password

  con.query('UPDATE mst_employee SET id_employee=?,name=? ,surname=?,position=?,salary=?,total_sale=? WHERE id_employee=?',
   [search, c,s,position,salary,total_sale,search], (err, res4) => {
    if (err) throw err
  })
  con.query('UPDATE mst_security SET id_security=?, user=? ,password=?,id_employee=? WHERE id_employee=?',
   [search, user,pass ,search, search], (err, res4) => {
    if (err) throw err
  })
  res1.redirect('/main')
})



app.get('/deletemember.html', function (req, res, next) {
  res.sendFile(__dirname + '/html/' + 'deletemember.html')
})
var search1
app.get('/delete/:idx', function (req, res, next) {
  let { idx } = req.params
  search1 = idx
  con.query(
    'SELECT mst_employee.id_employee, name, surname, position, FORMAT(salary,2) AS salary , FORMAT(total_sale,2) as total_sale FROM  mst_employee where id_employee = ? ',
  search1,
    (err, datam) => {
      if (err) throw err
      console.log(datam)
      res.render(__dirname + '/html/deletemember.html', { 
        datam: datam})
    }
  )
})
app.post('/delete', function (req, res1, next) {
  console.log("ye s")
  
  con.query('DELETE FROM mst_security WHERE mst_security.id_employee=?',[search1], (err, resx4) => {
    if (err) throw err
    console.log("yes")
  })
  con.query('DELETE FROM trn_login WHERE trn_login.id_employee=?',[search1], (err, resx6) => {
    if (err) throw err
    console.log("yes")
  })
  con.query('DELETE FROM trn_logout WHERE trn_logout.id_employee=?',[search1], (err, resx7) => {
    if (err) throw err
    console.log("yes")
  })
  con.query('DELETE FROM mst_employee WHERE mst_employee.id_employee=?',[search1], (err, resx5) => {
    if (err) throw err
    console.log("yes")
  })
  res1.redirect('/main')
})
